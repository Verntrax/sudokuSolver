package application;

import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.geometry.Pos;
import javafx.scene.text.Text;
import solver.Solver;

import java.util.HashSet;
import java.util.ArrayList;

public class Controller {
    private Solver solver;
    private int[][] puzzle;

    public GridPane grid;
    public Text info;

    private TextField[][] fields;

    public void initialize() {
        solver = new Solver();
        puzzle = new int[Solver.SIZE][Solver.SIZE];
        fields = new TextField[Solver.SIZE][Solver.SIZE];

        createFields();
    }

    private void createFields() {
        for (int i = 0; i < Solver.SIZE; ++i) {
            for(int j = 0; j < Solver.SIZE; ++j) {
                fields[i][j] = new TextField();
                fields[i][j].setAlignment(Pos.CENTER);
                fields[i][j].setMinSize(30, 30);
                fields[i][j].setMaxSize(30, 30);
                fields[i][j].setFont(Font.font("Tahoma", FontWeight.NORMAL, 15));
                grid.add(fields[i][j], i + (i/3)*2, j + (j/3)*2 + 2);
            }
        }
    }

    private boolean validatePuzzle() {
        ArrayList<HashSet<Integer>> rows = new ArrayList<>();
        ArrayList<HashSet<Integer>> columns = new ArrayList<>();
        ArrayList<HashSet<Integer>> areas = new ArrayList<>();

        boolean result = true;

        for(int i = 0; i < Solver.SIZE; ++i) {
            rows.add(new HashSet<>());
            columns.add(new HashSet<>());
            areas.add(new HashSet<>());
        }
        for(int i = 0; i < Solver.SIZE; ++i) {
            for(int j = 0; j < Solver.SIZE; ++j) {
                if(puzzle[i][j] != 0) {
                    if(!rows.get(i).add(puzzle[i][j])) {
                        result = false;
                        break;
                    }
                    if(!columns.get(j).add(puzzle[i][j])) {
                        result = false;
                        break;
                    }
                    if(!areas.get((i/3)*3+j/3).add(puzzle[i][j])) {
                        result = false;
                        break;
                    }
                }
            }
        }
        return result;
    }

    public void clearFields() {
        info.setText("");
        for(int i = 0; i < Solver.SIZE; ++i) {
            for(int j = 0; j < Solver.SIZE; ++j) {
                fields[i][j].clear();
                fields[i][j].setEditable(true);
                fields[i][j].setStyle("-fx-control-inner-background: white");
            }
        }
    }

    private boolean inputIsCorrect() {
        boolean isCorrect = true;

        for (int i = 0; i < Solver.SIZE; ++i) {
            for(int j = 0; j < Solver.SIZE; ++j) {
                String content = fields[i][j].getCharacters().toString();
                if((content.matches("[1-9]"))) {
                    puzzle[i][j] = Integer.parseInt(content);
                } else if(content.isEmpty()) {
                    puzzle[i][j] = 0;
                } else {
                    fields[i][j].setStyle("-fx-control-inner-background: red");
                    isCorrect = false;
                }
            }
        }
        return isCorrect;
    }

    private void printSolution() {
        for(int i = 0; i < Solver.SIZE; ++i) {
            for(int j = 0; j < Solver.SIZE; ++j) {
                fields[i][j].setEditable(false);
                if(puzzle[i][j] != 0) {
                    fields[i][j].setStyle("-fx-control-inner-background: gainsboro");
                }
            }
        }
        puzzle = solver.getPuzzle();

        for(int i = 0; i < Solver.SIZE; ++i) {
            for(int j = 0; j < Solver.SIZE; ++j) {
                fields[i][j].setText(((Integer)(puzzle[i][j])).toString());
            }
        }
    }

    public void solveSudoku() {

        for (int i = 0; i < Solver.SIZE; ++i) {
            for (int j = 0; j < Solver.SIZE; ++j) {
                fields[i][j].setStyle("-fx-control-inner-background: white");
            }
        }
        boolean isCorrect = inputIsCorrect();

        if (!isCorrect) {
            info.setFill(Color.FIREBRICK);
            info.setText("Incorrect data inserted");
        } else {
            solver.setPuzzle(puzzle);

            if(!validatePuzzle()) {
                info.setFill(Color.FIREBRICK);
                info.setText("Invalid puzzle");
            } else {
                solver.setPuzzle(puzzle);

                long beginTime = System.currentTimeMillis();
                boolean solved = solver.solvePuzzle();
                long endTime = System.currentTimeMillis();

                if (solved) {
                    printSolution();

                    info.setFill(Color.BLACK);
                    info.setText("Puzzle solved.\nEvaluation time: " + (endTime - beginTime) + " ms");
                } else {
                    info.setFill(Color.FIREBRICK);
                    info.setText("No solution exists");
                }
            }
        }
    }
}
