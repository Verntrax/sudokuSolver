package solver;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Stack;

public class Solver {
    public static final int SIZE = 9;

    private class EmptyField {
        private final HashSet<Integer> possibilities;
        private final int row;
        private final int column;
        private EmptyField(int row, int column) {
            possibilities = new HashSet<>();
            this.row = row;
            this.column = column;
        }
        @Override
        public int hashCode() {
            return 10*(row) + column;
        }
    }

    private final int[][] puzzle;
    private final HashSet<EmptyField> emptyFields;
    private final ArrayList<HashSet<EmptyField>> emptyFieldsRows;
    private final ArrayList<HashSet<EmptyField>> emptyFieldsColumns;
    private final ArrayList<HashSet<EmptyField>> emptyFieldsAreas;

    private final Stack<EmptyField> testStack;
    private final ArrayList<ArrayList<Integer>> testRows;
    private final ArrayList<ArrayList<Integer>> testColumns;
    private final ArrayList<ArrayList<Integer>> testAreas;

    public Solver() {
        puzzle = new int[Solver.SIZE][Solver.SIZE];
        emptyFields = new HashSet<>();
        emptyFieldsRows = new ArrayList<>(Solver.SIZE);
        emptyFieldsColumns = new ArrayList<>(Solver.SIZE);
        emptyFieldsAreas = new ArrayList<>(Solver.SIZE);

        testStack = new Stack<>();
        testRows = new ArrayList<>(Solver.SIZE);
        testColumns = new ArrayList<>(Solver.SIZE);
        testAreas = new ArrayList<>(Solver.SIZE);

        for (int i = 0; i < Solver.SIZE; ++i) {
            emptyFieldsRows.add(new HashSet<>());
            emptyFieldsColumns.add(new HashSet<>());
            emptyFieldsAreas.add(new HashSet<>());

            testRows.add(new ArrayList<>());
            testColumns.add(new ArrayList<>());
            testAreas.add(new ArrayList<>());
        }
    }

    private int computeAreaNumber(int row, int column) {
        return (row/3)*3 + column/3;
    }

    private void clearFields() {
        emptyFields.clear();
        testStack.clear();

        for (int i =0; i < Solver.SIZE; ++i) {
            emptyFieldsRows.get(i).clear();
            emptyFieldsColumns.get(i).clear();
            emptyFieldsAreas.get(i).clear();

            testRows.get(i).clear();
            testColumns.get(i).clear();
            testAreas.get(i).clear();
        }
    }

    public void setPuzzle(int[][] toSolve) {
        clearFields();
        for (int i = 0; i < Solver.SIZE; ++i) {
            System.arraycopy(toSolve[i], 0, puzzle[i], 0, Solver.SIZE);
        }
        for (int i = 0; i < Solver.SIZE; ++i) {
            for (int j = 0; j < Solver.SIZE; ++j) {
                if (puzzle[i][j] == 0) {
                    EmptyField emptyField = new EmptyField(i, j);

                    emptyFields.add(emptyField);
                    emptyFieldsRows.get(i).add(emptyField);
                    emptyFieldsColumns.get(j).add(emptyField);
                    emptyFieldsAreas.get(computeAreaNumber(i, j)).add(emptyField);
                }
            }
        }
    }

    public int[][] getPuzzle() {
        int[][] result = new int[Solver.SIZE][Solver.SIZE];

        for (int i = 0; i < Solver.SIZE; ++i) {
            System.arraycopy(puzzle[i], 0, result[i], 0, Solver.SIZE);
        }
        return result;
    }

    private void clearSection(HashSet<EmptyField> section, int element) {

        for (EmptyField emptyField : section) {
            emptyField.possibilities.remove(element);
        }
    }

    private void findPossibilities() {

        for (EmptyField emptyField : emptyFields) {
            for (int i = 1; i <= Solver.SIZE; ++i) {
                emptyField.possibilities.add(i);
            }
        }
        for (int i = 0; i < Solver.SIZE; ++i) {
            for (int j = 0; j < Solver.SIZE; ++j) {
                int element = puzzle[i][j];

                if (element != 0) {
                    clearSection(emptyFieldsRows.get(i), element);
                    clearSection(emptyFieldsColumns.get(j), element);
                    clearSection(emptyFieldsAreas.get(computeAreaNumber(i, j)), element);
                }
            }
        }
    }

    private boolean tryElement(EmptyField field, HashSet<EmptyField> section, int element) {

        for (EmptyField computedField : section) {
            if (computedField != field) {
                if (computedField.possibilities.contains(element)) {
                    return false;
                }
            }
        }
        return true;
    }

    private int tryFill(EmptyField field) {
        int solution = 0;
        int row = field.row;
        int column = field.column;
        int area = computeAreaNumber(row, column);

        if (field.possibilities.size() == 1) {
            solution = field.possibilities.iterator().next();
        } else {
            for (Integer element : field.possibilities) {
                if (tryElement(field, emptyFieldsRows.get(row), element) ||
                        tryElement(field, emptyFieldsColumns.get(column), element) ||
                        tryElement(field, emptyFieldsAreas.get(area), element)) {

                    solution = element;
                    break;
                }
            }
        }
        return solution;
    }

    private void fill(EmptyField field, int solution) {
        int row = field.row;
        int column = field.column;
        int area = computeAreaNumber(row, column);

        puzzle[row][column] = solution;
        emptyFields.remove(field);
        emptyFieldsRows.get(row).remove(field);
        emptyFieldsColumns.get(column).remove(field);
        emptyFieldsAreas.get(area).remove(field);

        for (EmptyField computedField : emptyFieldsRows.get(row)) {
            computedField.possibilities.remove(solution);
        }
        for (EmptyField computedField : emptyFieldsColumns.get(column)) {
            computedField.possibilities.remove(solution);
        }
        for (EmptyField computedField : emptyFieldsAreas.get(area)) {
            computedField.possibilities.remove(solution);
        }
    }

    private boolean solveClever() {
        Iterator<EmptyField> fieldIterator = emptyFields.iterator();

        while(fieldIterator.hasNext()) {
            EmptyField field = fieldIterator.next();

            int solution = tryFill(field);
            if (solution != 0) {
                fill(field, solution);
                fieldIterator = emptyFields.iterator();
            }
        }
        return emptyFields.isEmpty();
    }

    private boolean fillBrutal() {
        if (testStack.empty()) {
            return true;
        }
        EmptyField field = testStack.pop();
        int row = field.row;
        int column = field.column;
        int area = computeAreaNumber(row, column);

        for (Integer element : field.possibilities) {
            if (testRows.get(row).contains(element)) {
                continue;
            }
            if (testColumns.get(column).contains(element)) {
                continue;
            }
            if (testAreas.get(area).contains(element)) {
                continue;
            }
            testRows.get(row).add(element);
            testColumns.get(column).add(element);
            testAreas.get(area).add(element);

            if (fillBrutal()) {
                puzzle[row][column] = element;
                return true;
            }
            Integer integerElement = element;
            testRows.get(row).remove(integerElement);
            testColumns.get(column).remove(integerElement);
            testAreas.get(area).remove(integerElement);
        }
        testStack.push(field);
        return false;
    }

    private boolean solveBrutal() {
        for (EmptyField field : emptyFields) {
            testStack.push(field);
        }
        if (fillBrutal()) {
            clearFields();
            return true;
        }
        return false;
    }

    public boolean solvePuzzle() {
        findPossibilities();

        if (!solveClever()) {
            if (!solveBrutal()) {
                return false;
            }
        }
        return true;
    }

    /*public void printPuzzle(){
        System.out.println();

        for(int i = 0; i < Solver.SIZE; i++){
            for(int j = 0; j < Solver.SIZE; j++){
                System.out.print(puzzle[i][j]+" ");

                if(j%3 == 2){
                    System.out.print(" ");
                }
            }
            System.out.println();

            if(i%3 == 2){
                System.out.println();
            }
        }
    }*/
}
